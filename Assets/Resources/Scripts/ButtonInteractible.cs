﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

namespace IEVA_Projet
{
    public class ButtonInteractible : MonoBehaviourPunCallbacks
    {
        [Tooltip("This boolean indicates whether it is time limited or not")]
        public bool isStatic;
        [Tooltip("Time during the lever stays active (useless if isStatic enabled)")]
        public float duration;
        public List<GameObject> lightPanes;
        public Animator animator;

        private bool isActive;
        private List<Renderer> renderers;
        [SerializeField]
        private DoorController door;

        public bool IsActive
        {
            get { return this.isActive; }
            set { this.isActive = value; }
        }

        // Start is called before the first frame update
        void Start()
        {
            this.isActive = false;
            this.renderers = new List<Renderer>();
            foreach (GameObject item in lightPanes) {
                renderers.Add(item.GetComponent<Renderer>());
            }
            
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void Activate()
        {
            this.isActive = true;
            UpdateVisualStatus();
            photonView.RPC("SetActive", RpcTarget.All, this.photonView.ViewID, true);

            if(!isStatic) {
                StartCoroutine("RunDuring");
            }
            
        }

        public void UpdateVisualStatus()
        {
            if (isActive) {
                this.animator.SetBool("IsActive", true);
                foreach (Renderer item in renderers) {
                    item.material.SetColor("_EmissionColor", Color.green);
                }

            } else {
                this.animator.SetBool("IsActive", false);
                foreach (Renderer item in renderers) {
                    item.material.SetColor("_EmissionColor", Color.red);
                }

            }

        }

        IEnumerator RunDuring()
        {
            print("Start Coroutine");
            yield return new WaitForSeconds(duration);

            this.isActive = false;
            UpdateVisualStatus();

            photonView.RPC("SetActive", RpcTarget.All, this.photonView.ViewID, false);
            print("Coroutine complete");
        }

        [PunRPC]
        public void SetActive(int buttonID, bool active)
        {
            door.SwitchState(buttonID, active);
        }
    }
}
