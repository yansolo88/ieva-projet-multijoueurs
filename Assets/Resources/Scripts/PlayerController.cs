﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using System;

namespace IEVA_Projet
{
    public enum PLAYER_STATE
    {
        IDLE,
        WALKING,
        FIRING,
        HOLDING
    }

    public class PlayerController : MonoBehaviourPunCallbacks
    {
        #region Public Fields
        [Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
        public static GameObject LocalPlayerInstance;
        public Camera localCamera;
        public Canvas localUI;

        #endregion

        #region Private Fields
        //private string playerName;
        //private PlayerController otherPlayer = null;
        private PLAYER_STATE m_state;
        private bool m_tooFar = true;
        private Cargo target = null;
        private Transform associatedTargetPlaceholder;
        [SerializeField]
        private Text interactibleUI;
        [SerializeField]
        private Text timerUI;
        private GameManager gm;


        #endregion

        #region Properties
        public PLAYER_STATE State
        {
            get { return this.m_state; }
            set { this.m_state = value; }
        }

        public bool IsTooFar
        {
            get { return m_tooFar; }
        }

        //public string Nickname
        //{
        //    get { return this.playerName; }
        //}
        #endregion

        #region Unity Methods
        private void Awake()
        {
            /* 
             * #Important
             * used in GameManager.cs: we keep track of the localPlayer instance to prevent instantiation when levels are synchronized
             */
            if (photonView.IsMine) {
                LocalPlayerInstance = this.gameObject;
            }

            /* 
             * #Critical
             * we flag as don't destroy on load so that instance survives level synchronization, thus giving a seamless experience when levels load.
             */
            //DontDestroyOnLoad (this.gameObject) ;
        }

        // Start is called before the first frame update
        private void Start()
        {
            if (photonView.IsMine) {// || ! PhotonNetwork.IsConnected) {
                if (localCamera == null) {
                    Debug.LogError("<Color=Red><a>Missing</a></Color> camera Reference. Please set it up in GameObject 'Player'", this);
                } else {
                    localCamera.gameObject.SetActive(true);
                    localUI.gameObject.SetActive(true);
                }

                this.interactibleUI.text = "";

                this.m_state = PLAYER_STATE.IDLE;

                this.gm = FindObjectOfType<GameManager>();
                this.gm.timer.UIDisplay = timerUI;

            }

        }

        // Update is called once per frame
        private void Update()
        {
            CheckTargetTooFar();

            if (m_tooFar && target != null) {
                print("----- Too far from the cargo ; releasing !");
                Release(target.gameObject);
            }
        }

        #endregion

        #region Public Methods
        public void Catch(GameObject target)
        {
            if (target != null && this.State != PLAYER_STATE.HOLDING) {
                Cargo tb = target.GetComponentInParent<Cargo>();
                List<HoldableHelper> placeHolders = tb.handles;

                associatedTargetPlaceholder = NearestPlaceholder(placeHolders);
                
                if (tb != null) {
                    print("Catch() : AddOwner of object " + tb.photonView.ViewID + " to " + photonView.ViewID);
                    this.gameObject.transform.position = associatedTargetPlaceholder.position;

                    this.interactibleUI.text = "";

                    photonView.RPC("AddOwner", RpcTarget.All, tb.photonView.ViewID, photonView.ViewID);
                    PhotonNetwork.SendAllOutgoingCommands();

                    print("----- Object caught !");
                    this.target = tb;
                }
            } else {
                print("----- Sapce : Too far from any object");
            }
        }

        public void Release(GameObject target)
        {
            if (target != null) {
                Cargo tb = target.GetComponent<Cargo>();
                if (tb != null) {
                    print("Release() : RemoveOwner of object " + tb.photonView.ViewID + " to " + photonView.ViewID);
                    photonView.RPC("RemoveOwner", RpcTarget.All, tb.photonView.ViewID, photonView.ViewID);
                    PhotonNetwork.SendAllOutgoingCommands();

                    print("----- Object released !");
                    
                }
            }
        }

        //public void SetChildObject(int targetID, bool set)
        //{
        //    Cargo c = PhotonView.Find(targetID).gameObject.GetComponent<Cargo>();

        //    if(set) {
        //        if(c.transform.parent == null) {
        //            c.transform.SetParent(this.transform);
        //            this.State = PLAYER_STATE.HOLDING;
        //        }
        //    } else {
        //        if(c.transform.parent != null) {
        //            c.transform.SetParent(null);
        //            this.State = PLAYER_STATE.IDLE;
        //        }
        //    }

        //}

        //public void UpdateTooFar(bool isTooFar)
        //{
        //    this.m_tooFar = isTooFar;
        //}

        public void UpdateInteractibleUI(string message)
        {
            this.interactibleUI.text = message;
        }

        #endregion

        #region Private Methods
        private void CheckTargetTooFar()
        {
            if(target != null) {
                float dist = Vector3.Distance(
                    Vector3.ProjectOnPlane(target.transform.position, new Vector3(0, 1, 0)),
                    Vector3.ProjectOnPlane(this.transform.position, new Vector3(0, 1, 0))
                );

                if (dist < target.m_maxDistFromTarget) {
                    if (m_tooFar) { // ne s'exécute que si le joueur est trop loin
                        m_tooFar = false;
                        print("----- In range of cargo");
                    }

                } else {
                    if (!m_tooFar) { // ne s'exécute que si le joueur n'est pas trop loin
                        m_tooFar = true;
                        print("----- Out of range of cargo");
                    }
                }

            }
        }

        private Transform NearestPlaceholder(List<HoldableHelper> p)
        {
            Transform result = this.transform;
            float distMin = float.MaxValue;

            foreach (HoldableHelper item in p) {
                Transform ph = item.placeHolder;
                float tmp = Vector3.Distance(this.gameObject.transform.position, ph.position);
                if (tmp < distMin) {
                    distMin = tmp;
                    result = ph;
                }
            }

            return result;
        }

        #endregion

        #region PUN Methods
        [PunRPC]
        public void AddOwner(int interactiveID, int newOwnerID)
        {
            Cargo target = PhotonView.Find(interactiveID).gameObject.GetComponent<Cargo>();
            MonoBehaviourPun owner = PhotonView.Find(newOwnerID).gameObject.GetComponent<MonoBehaviourPun>();
            print("AddOwner of object " + target.name + " : " + owner.name);

            bool res = target.SetOwner(owner);
            if(res) {
                this.State = PLAYER_STATE.HOLDING;
            }

        }

        [PunRPC]
        public void RemoveOwner(int interactiveID, int newOwnerID)
        {
            Cargo target = PhotonView.Find(interactiveID).gameObject.GetComponent<Cargo>();
            MonoBehaviourPun owner = PhotonView.Find(newOwnerID).gameObject.GetComponent<MonoBehaviourPun>();
            print("RemoveOwner of object " + target.name + " : " + owner.name);
            bool res = target.RemoveOwner(owner);
			
            if (res) {
                this.target = null;
                this.State = PLAYER_STATE.IDLE;
            }

        }

        #endregion

    }

}
