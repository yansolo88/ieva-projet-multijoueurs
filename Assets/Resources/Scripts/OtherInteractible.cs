﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IEVA_Projet
{
    public class OtherInteractible : Interactive
    {
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnTriggerEnter(Collider other)
        {
            print(name + " : OtherInteractible.OnCollisionEnter()");
            var hit = other.gameObject;
            var cursor = hit.GetComponent<CursorTool>();
            if (cursor != null) {
                Renderer renderer = GetComponentInChildren<Renderer>();
                renderer.material.color = Color.blue;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            print(name + " : OtherInteractible.OnCollisionExit()");
            var hit = other.gameObject;
            var cursor = hit.GetComponent<CursorTool>();
            if (cursor != null) {
                Renderer renderer = GetComponentInChildren<Renderer>();
                renderer.material.color = Color.white;
            }
        }

    }
}
