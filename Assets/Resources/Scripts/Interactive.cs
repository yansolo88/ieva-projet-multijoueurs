﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

namespace IEVA_Projet
{
    public class Interactive : MonoBehaviourPun {
        // Start is called before the first frame update

        MonoBehaviourPun owner = null ;

        private void Start () {
        
        }

        // Update is called once per frame
        private void Update () {
        
        }

        public void SetOwner (MonoBehaviourPun owner) {
            this.owner = owner ;
            if (owner != null) {
                transform.SetParent (owner.transform) ;
            } else {
                transform.SetParent (null) ;
            }
        }

        public void RemoveOwner () {
            transform.SetParent (null) ;
            owner = null ;
        }

        public MonoBehaviourPun GetOwner () {
            return owner ;
        }
    }

}
