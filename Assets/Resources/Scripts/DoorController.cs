﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

namespace IEVA_Projet {
    public class DoorController : MonoBehaviourPun
    {
        public List<ButtonInteractible> levers;
        public Animator animator;
        public bool forceOpen;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            int cpt = 0;
            if(levers.Count > 0) {
                foreach (ButtonInteractible item in levers) {
                    if (item.IsActive) {
                        cpt++;
                    }
                }
            }

            if (forceOpen) {
                this.animator.SetBool("IsOpen", true);
            } else {
                if (cpt == levers.Count) {
                    this.animator.SetBool("IsOpen", true);
                } else {
                    this.animator.SetBool("IsOpen", false);
                }
            }
        }

        public void SwitchState(int buttonID, bool newState)
        {
            foreach (ButtonInteractible item in levers) {
                if(item.photonView.ViewID == buttonID) {
                    item.IsActive = newState;
                    item.UpdateVisualStatus();
                }
            }

        }
    }
}
