﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

namespace IEVA_Projet
{
    public class CursorTool : MonoBehaviour
    {
        float x, previousX = 0;
        float y, previousY = 0;
        float z, lastZ;
        public bool active;

        private bool holding;
        [SerializeField]
        private GameObject target;
        MonoBehaviourPun targetParent;

        //private MonoBehaviourPun player;
        private PlayerController player;

        #region Public Fields
        #endregion

        #region Private Fields
        #endregion

        #region Properties
        #endregion

        #region Unity Methods
        private void Start()
        {
            active = false;
            holding = false;
            //player = (MonoBehaviourPun)this.GetComponentInParent(typeof(PlayerController));
            player = this.GetComponentInParent<PlayerController>();
            name = player.name + "_" + name;
        }

        private void Update()
        {
            // control of the 3D cursor
            if (player.photonView.IsMine || !PhotonNetwork.IsConnected) {
                //if (Input.GetButtonDown ("Fire1")) {
                //	Fire1Pressed (Input.mousePosition.x, Input.mousePosition.y) ;
                //}
                //if (Input.GetButtonUp ("Fire1")) {
                //	Fire1Released (Input.mousePosition.x, Input.mousePosition.y) ;
                //}
                //if (active) {
                //	Fire1Moved (Input.mousePosition.x, Input.mousePosition.y, Input.mouseScrollDelta.y) ;
                //}
                if (Input.GetKeyDown(KeyCode.Space)) {
                    if (player.State != PLAYER_STATE.HOLDING) {
                        player.Catch(target);
                        holding = true;
                    }
                }
                if (Input.GetKeyUp(KeyCode.Space)) {
                    player.Release(target);
                    
                    target = null;
                    holding = false;
                }
                if (Input.GetKeyDown(KeyCode.E)) {
                    if (target != null && !target.GetComponent<ButtonInteractible>().IsActive) {
                        target.GetComponent<ButtonInteractible>().Activate();
                        player.UpdateInteractibleUI("");
                    }
                }
            }

        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Holdable" && target == null) {
                print(name + " : OnTriggerEnter() - " + other.name + " attached to " + other.transform.parent.name);
                target = other.transform.parent.transform.parent.gameObject;
                other.gameObject.GetComponent<Renderer>().material.color = Color.blue;
                int remain = other.gameObject.GetComponentInParent<Cargo>().Remaining;

                player.UpdateInteractibleUI("Space (hold)..... Grab payload (" + remain +" required)");
            }

            if(other.tag == "Interactible") {
                target = other.gameObject;
                if (!target.GetComponent<ButtonInteractible>().IsActive) {
                    player.UpdateInteractibleUI("E......... Interact");
                }
            }
        }

        private void OnTriggerStay(Collider other)
        {
            if (other.tag == "Holdable" && target == null) {
                print(name + " : OnTriggerStay() - " + other.name);
                target = other.transform.parent.gameObject;
                other.gameObject.GetComponent<Renderer>().material.color = Color.blue;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.tag == "Holdable" && target != null) {
                print(name + " : OnTriggerExit()");
                player.UpdateInteractibleUI("");

                if (!holding) {
                    target = null;
                }
                other.gameObject.GetComponent<Renderer>().material.color = Color.white;
            }

            if (other.tag == "Interactible") {
                target = null;
                player.UpdateInteractibleUI("");
            }

        }

        #endregion

        #region Public Methods
        public void Fire1Pressed(float mouseX, float mouseY)
        {
            active = true;
            x = mouseX;
            previousX = x;
            y = mouseY;
            previousY = y;
        }

        public void Fire1Released(float mouseX, float mouseY)
        {
            active = false;
        }

        public void Fire1Moved(float mouseX, float mouseY, float mouseZ)
        {
            x = mouseX;
            float deltaX = (x - previousX) / 100.0f;
            previousX = x;
            y = mouseY;
            float deltaY = (y - previousY) / 100.0f;
            previousY = y;
            float deltaZ = mouseZ / 10.0f;
            transform.Translate(deltaX, deltaY, deltaZ);
        }

        #endregion

        #region Private Methods
        #endregion

        #region PUN Methods
        //[PunRPC]
        //public void AddOwner(int interactiveID, int newOwnerID)
        //{
        //    Cargo target = PhotonView.Find(interactiveID).gameObject.GetComponent<Cargo>();
        //    MonoBehaviourPun owner = PhotonView.Find(newOwnerID).gameObject.GetComponent<MonoBehaviourPun>();
        //    print("AddOwner of object " + target.name + " : " + owner.name);
        //    target.SetOwner(owner);
        //}

        //[PunRPC]
        //public void RemoveOwner(int interactiveID, int newOwnerID)
        //{
        //    Cargo target = PhotonView.Find(interactiveID).gameObject.GetComponent<Cargo>();
        //    MonoBehaviourPun owner = PhotonView.Find(newOwnerID).gameObject.GetComponent<MonoBehaviourPun>();
        //    print("RemoveOwner of object " + target.name + " : " + owner.name);
        //    target.RemoveOwner(owner);
        //}

        #endregion

        //public void Catch() {
        //    if (target != null && target.tag == "Holdable" && !holding) {
        //        Cargo tb = target.GetComponent<Cargo>();

        //        if (tb != null) {
        //            //if ((!holding) && (this != tb.GetOwner())) { // pour ne pas prendre 2 fois l'objet et lui faire perdre son parent
        //            //    targetParent = tb.GetOwner();
        //            //}
        //            print("AddOwner of object " + tb.photonView.ViewID + " to " + photonView.ViewID);
        //            photonView.RPC("AddOwner", RpcTarget.All, tb.photonView.ViewID, photonView.ViewID);
        //            PhotonNetwork.SendAllOutgoingCommands();

        //            holding = true;

        //            //Quaternion holdingOrientation = this.GetComponentInParent<PlayerController>().transform.rotation;
        //            //this.GetComponentInParent<RigidbodyFirstPersonController>().mouseLook.UpdateCameraClamping(holdingOrientation);

        //            print("Object caught !");
        //        }
        //    } else {
        //        print("Sapce : Too far from any object");
        //    }
        //}

        //public void Release()
        //{
        //    if (target != null) {
        //        Cargo tb = target.GetComponent<Cargo>();
        //        if (tb != null) {
        //            //if (targetParent != null) {
        //            //    photonView.RPC("AddOwner", RpcTarget.All, tb.photonView.ViewID, targetParent.photonView.ViewID);
        //            //    targetParent = null;
        //            //} else {
        //            //    photonView.RPC("RemoveOwner", RpcTarget.All, tb.photonView.ViewID);
        //            //}
        //            //PhotonNetwork.SendAllOutgoingCommands();

        //            print("RemoveOwner of object " + tb.photonView.ViewID + " to " + photonView.ViewID);
        //            photonView.RPC("RemoveOwner", RpcTarget.All, tb.photonView.ViewID, photonView.ViewID);
        //            PhotonNetwork.SendAllOutgoingCommands();
        //            holding = false;

        //            //this.GetComponentInParent<RigidbodyFirstPersonController>().mouseLook.UpdateCameraClamping(Quaternion.identity);

        //            print("Object released !");
        //            target = null;
        //        }
        //    }
        //}



    }
}