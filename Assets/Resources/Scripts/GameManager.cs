﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;


namespace IEVA_Projet
{
    public class GameManager : MonoBehaviourPunCallbacks {

        #region Public Fields

        public static GameManager Instance ;

        [Tooltip("The prefab to use for representing the player")]
        public PlayerController playerPrefab;
        public Cargo target;
        public TimerController timer;
        public AudioSource audio;
        public AudioClip tenseMusic;

        private bool sceneIsLoading = false;
        private GameObject localPlayer;
        #endregion

        private void Start () {
            Instance = this ;
            if (playerPrefab == null) {
                Debug.LogError ("<Color=Red><a>Missing</a></Color> playerPrefab Reference. Please set it up in GameObject 'Game Manager'", this) ;
            } else {
                //im.Player = playerPrefab;
                //Debug.LogFormat("We are Instantiating LocalPlayer from {0}", Application.loadedLevelName);
                // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
                if (PlayerController.LocalPlayerInstance == null) {
                    Debug.LogFormat ("We are Instantiating LocalPlayer from {0}", SceneManagerHelper.ActiveSceneName) ;
                    // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
                    localPlayer = PhotonNetwork.Instantiate (this.playerPrefab.name, new Vector3 (0f, 1.0f, 0f), Quaternion.identity, 0);
                } else {
                    Debug.LogFormat ("Ignoring scene load for {0}", SceneManagerHelper.ActiveSceneName) ;
                }
            }

        }

        #region Private Methods

        #endregion

        #region Photon Callbacks

        /// <summary>
        /// Called when the local player left the room. We need to load the launcher scene.
        /// </summary>
        public override void OnLeftRoom () {
            SceneManager.LoadScene (0) ;
        }

        public override void OnPlayerEnteredRoom (Player other) {
            Debug.LogFormat ("OnPlayerEnteredRoom() {0}", other.NickName) ; // not seen if you're the player connecting
            // we load the Arena only once, for the first user who connects, it is made by the launcher

            if (PhotonNetwork.IsMasterClient) {
                Debug.LogFormat ("OnPlayerEnteredRoom IsMasterClient {0}", PhotonNetwork.IsMasterClient) ; // called before OnPlayerLeftRoom
            }

            Camera[] cameras = Camera.allCameras;
            foreach (Camera c in cameras) {
                Transform p = c.transform.parent;
                PhotonView pv = p.GetComponentInParent<PhotonView>();

                if (!pv.Owner.IsLocal) {
                    pv.GetComponentInChildren<Canvas>().gameObject.SetActive(false);
                    c.gameObject.SetActive(false);
                }
            }

        }

        public override void OnPlayerLeftRoom (Player other) {
            Debug.LogFormat ("OnPlayerLeftRoom() {0}", other.NickName) ; // seen when other disconnects
        }

        [PunRPC]
        public void StartTimer()
        {
            if (!this.timer.IsRunning) {
                this.timer.StartTimer();
                this.audio.Stop();
                this.audio.clip = tenseMusic;
                this.audio.Play();
            }
            
        }
        #endregion

        #region Public Methods

        // called by leave button on UI
        public void LeaveRoom () {
            localPlayer.GetComponent<RigidbodyFirstPersonController>().mouseLook.SetCursorLock(false);
            PhotonNetwork.LeaveRoom();
        }

        public void StartGame()
        {
            photonView.RPC("StartTimer", RpcTarget.All);
        }

        public void FinishGame()
        {
            localPlayer.GetComponent<RigidbodyFirstPersonController>().mouseLook.SetCursorLock(false);
            if (PhotonNetwork.IsMasterClient && !sceneIsLoading) {
                PhotonNetwork.LoadLevel("Victory");
                sceneIsLoading = true;
            }

        }

        public void GameOver()
        {
            localPlayer.GetComponent<RigidbodyFirstPersonController>().mouseLook.SetCursorLock(false);
            if (PhotonNetwork.IsMasterClient && !sceneIsLoading) {
                PhotonNetwork.LoadLevel("GameOver");
                sceneIsLoading = true;
            }
            
        }

        #endregion

    }
}