﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

namespace IEVA_Projet
{
    public class Cargo : MonoBehaviourPun
    {
        #region Public Fields
        [Tooltip("Distance maximum avant qu'un joueur soit trop éloigné de la cible")]
        public float m_maxDistFromTarget;
        public List<HoldableHelper> handles;
        public Color unmovable;
        public Color movable;

        #endregion

        #region Private Fields
        private List<MonoBehaviourPun> ownersList;
        private int remaining;
        //private bool set = false;
        private int maxCapacity = 2;
        [SerializeField]
        private GameObject cargoMesh;

        #endregion

        #region Properties
        public int Remaining {
            get { return this.remaining; }
        }
        #endregion

        #region Unity Methods
        // Start is called before the first frame update
        private void Start() {
            this.ownersList = new List<MonoBehaviourPun>();
            this.remaining = maxCapacity;
            this.cargoMesh.GetComponent<Renderer>().material.color = unmovable;
        }

        // Update is called once per frame
        private void Update()
        {
            if(remaining == 0) {
                this.cargoMesh.GetComponent<Renderer>().material.color = movable;
                this.GetComponent<Rigidbody>().isKinematic = true;

                this.transform.position = CargoPosition();
                this.transform.rotation = CargoRotation();


            } else {
                this.cargoMesh.GetComponent<Renderer>().material.color = unmovable;
                this.GetComponent<Rigidbody>().isKinematic = false;
            }

        }

        private void OnTriggerEnter(Collider other) {
            if(other.tag == "Finish") {
                FindObjectOfType<GameManager>().FinishGame();
            }
            if (other.tag == "Start") {
                FindObjectOfType<GameManager>().StartGame();
                //other.gameObject.SetActive(false);
            }
        }

        #endregion

        #region Public Methods
        public bool SetOwner(MonoBehaviourPun owner) {
            if(remaining > 0) {
                this.ownersList.Add(owner);
                this.remaining--;
                NotifyRemaining();
            } else {
                Debug.LogWarning("Can't add more owners, list already full !");
            }

            return (this.ownersList.Find(e => e.Equals(owner)) != null);
        }

        public bool RemoveOwner(MonoBehaviourPun owner) {
            if(remaining == maxCapacity) {
                Debug.LogWarning("No owners listed ; no need to remove !");
            } else {
                this.ownersList.Remove(owner);
                //photonView.RPC("RemoveParent", RpcTarget.All, this.photonView.ViewID, owner.photonView.ViewID);
                //PhotonNetwork.SendAllOutgoingCommands();

                this.remaining++;
                NotifyRemaining();
            }

            return (this.ownersList.Find(e => e.Equals(owner)) == null);
        }

        #endregion

        #region Private Methods
        private void NotifyRemaining() {
            Debug.Log(remaining + "players required");
        }

        private Vector3 CargoPosition() {
            MonoBehaviourPun[] p = ownersList.ToArray();
            float newX = (p[0].gameObject.transform.position.x + p[1].gameObject.transform.position.x) / 2;
            float newZ = (p[0].gameObject.transform.position.z + p[1].gameObject.transform.position.z) / 2;

            return new Vector3(newX, 0.6f, newZ);
        }
		
		private Quaternion CargoRotation() {
			MonoBehaviourPun[] pArr = this.ownersList.ToArray();
			Quaternion q = this.transform.rotation;
			q.x /= q.w;
			q.y /= q.w;
			q.z /= q.w;
			q.w = 1.0f;

            float angle = (-1) * Mathf.Atan2( //result is in radians
                pArr[1].gameObject.transform.position.z - pArr[0].gameObject.transform.position.z,
                pArr[1].gameObject.transform.position.x - pArr[0].gameObject.transform.position.x
            );

			q.y = Mathf.Tan(0.5f * angle); //no need for conversion from degrees to radians

			return q;

		}

        #endregion

        #region PUN Methods
        //[PunRPC]
        //public void AddParent(int cargoID) {
        //    foreach (MonoBehaviourPun owner in ownersList) {
        //        PlayerController pc = owner.GetComponentInParent<PlayerController>();
        //        pc.SetChildObject(cargoID, true);
        //    }

        //}

        //[PunRPC]
        //public void RemoveParent(int cargoID, int ownerID) {
        //    MonoBehaviourPun owner = PhotonView.Find(ownerID).gameObject.GetComponent<MonoBehaviourPun>();

        //    PlayerController pc = owner.GetComponentInParent<PlayerController>();
        //    pc.SetChildObject(cargoID, false);

        //}

        #endregion

    }
}