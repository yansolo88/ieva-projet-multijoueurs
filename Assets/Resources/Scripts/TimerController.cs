﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;

namespace IEVA_Projet
{
    public class TimerController : MonoBehaviour
    {
        //timer duration (in seconds)
        [Tooltip("Timer duration in seconds")]
        public float duration;
        // text container to display the time remaining
        [Tooltip("UI Element to display the time remaining")]
        public Text UIDisplay;
        public List<AudioSource> speakers;
        public float volume;

        private bool running;
        private float countdown = float.MaxValue;
        private Stopwatch sw;

        public bool IsRunning
        {
            get { return this.running; }
        }

        public void Start()
        {
            this.running = false;
            this.sw = new Stopwatch();

            foreach (AudioSource item in speakers) {
                item.volume = this.volume;
            }

        }

        public void Update()
        {
            if (sw.ElapsedMilliseconds > 100) {
                countdown -= Mathf.Floor((float)sw.ElapsedMilliseconds / 100) / 10;
                Math.Round(countdown, 1);
                //print(countdown);
                sw.Restart();
            }

            if (running && countdown > 0) {
                UIDisplay.text = string.Format("{0:00}.{1:00}.{2:0}",
                    Mathf.Floor(countdown / 60),
                    Mathf.Floor(countdown % 60),
                    Mathf.Floor((countdown * 10) % 10)
                );

            } else {
                sw.Reset();
                this.running = false;
            }

            if(countdown < 0) {
                this.running = false;
                FindObjectOfType<GameManager>().GameOver();
            }

        }

        public void StartTimer()
        {
            UIDisplay.text = string.Format("{0:00}.{1:00}.{2:0}",
                Mathf.Floor(duration / 60),
                Mathf.Floor(duration % 60),
                Mathf.Floor((duration * 10) % 10)
            );

            sw.Start();
            running = true;
            countdown = duration;
            foreach (AudioSource item in speakers) {
                item.Play();
            }

        }

        public void ResetTimer()
        {
            this.running = false;
            foreach (AudioSource item in speakers) {
                item.Play();
            }

            sw.Reset();
            UIDisplay.text = string.Format("{0:00}.{1:00}.{2:0}",
                Mathf.Floor(0 / 60),
                Mathf.Floor(0 % 60),
                Mathf.Floor((0 * 10) % 10)
            );

        }

    }
}
