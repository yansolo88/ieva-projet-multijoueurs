﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CargoTest : MonoBehaviour
{
    public Transform from;
    public Transform to;
    public GameObject cargoMesh;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float newX = (from.position.x + to.position.x) / 2;
        float newZ = (from.position.z + to.position.z) / 2;
        cargoMesh.transform.position = new Vector3(newX, 0.0f, newZ);

        Quaternion q = cargoMesh.transform.rotation;
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;

        /*
        float angle = Vector3.Angle(
            new Vector3(from.position.x, 0.0f, from.position.z), 
            new Vector3(to.position.x, 0.0f, to.position.z)
        );
        */
        float angle = (-1) * Mathf.Atan2(
            to.position.z - from.position.z,
            to.position.x - from.position.x
        );

        print("Angle : " + angle * Mathf.Rad2Deg);

        q.y = Mathf.Tan(0.5f * angle);

        cargoMesh.transform.rotation = q;
    }
}
