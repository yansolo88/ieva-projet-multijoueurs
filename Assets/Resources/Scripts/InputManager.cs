using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

namespace IEVA_Projet
{
    public class InputManager : MonoBehaviourPun
    {
        #region Public Fields

        #endregion

        #region Private Fields
        private MonoBehaviourPun m_player;

        #endregion

        #region Properties
        public MonoBehaviourPun Player{
            set { this.m_player = value; }
        }

        #endregion

        #region Unity Methods
        private void Start() {
            if (this.m_player == null) {
                Debug.LogError("Player instance null !");
            }
        }

        private void Update() {
            if (m_player.photonView.IsMine || !PhotonNetwork.IsConnected) {
                //if (Input.GetButtonDown ("Fire1")) {
                //	Fire1Pressed (Input.mousePosition.x, Input.mousePosition.y) ;
                //}
                //if (Input.GetButtonUp ("Fire1")) {
                //	Fire1Released (Input.mousePosition.x, Input.mousePosition.y) ;
                //}
                
                // TODO : changer KeyCode.Space par une action dans les PlayerInputs
                // Space --> Hold
                // E --> Interact
                if (Input.GetKeyDown(KeyCode.Space)) {
                    Catch(null);
                }
                if (Input.GetKeyUp(KeyCode.Space)) {
                    Release(null);
                    //target = null;
                }
                if (Input.GetKeyDown(KeyCode.E)) {
                    Debug.Log("Not implemented yet !");
                }
            }

        }

        #endregion

        #region Public Methods
        public void Catch(GameObject other)
        {
            print("Space ?");

            if (other != null) {
                print("Space :");

                Cargo tb = other.GetComponent<Cargo>();

                if (tb != null) {
                    //if ((!caught) && (this != tb.GetOwner())) { // pour ne pas prendre 2 fois l'objet et lui faire perdre son parent
                    //    targetParent = tb.GetOwner();
                    //}
                    //print("AddOwner of object " + tb.photonView.ViewID + " to " + photonView.ViewID);
                    //photonView.RPC("AddOwner", RpcTarget.All, tb.photonView.ViewID, photonView.ViewID);
                    //tb.photonView.TransferOwnership(PhotonNetwork.LocalPlayer);
                    //PhotonNetwork.SendAllOutgoingCommands();

                    tb.SetOwner(this.GetComponentInParent<PlayerController>());
                    this.GetComponentInParent<PlayerController>().State = PLAYER_STATE.HOLDING;
                    //holding = true;

                    print("Object caught !");
                }
            } else {
                print("Too far from any object");
            }
        }

        public void Release(GameObject other)
        {
            if (other != null) {
                Cargo tb = other.GetComponent<Cargo>();
                if (tb != null) {
                    //if (targetParent != null) {
                    //    photonView.RPC("AddOwner", RpcTarget.All, tb.photonView.ViewID, targetParent.photonView.ViewID);
                    //    targetParent = null;
                    //} else {
                    //    photonView.RPC("RemoveOwner", RpcTarget.All, tb.photonView.ViewID);
                    //}
                    //PhotonNetwork.SendAllOutgoingCommands();

                    tb.RemoveOwner(this.GetComponentInParent<PlayerController>());
                    this.GetComponentInParent<PlayerController>().State = PLAYER_STATE.IDLE;
                    //holding = false;

                    print("Object released !");
                }
            }
        }

        [PunRPC]
        public void ChangeSupport(int interactiveID, int newSupportID)
        {
            Interactive go = PhotonView.Find(interactiveID).gameObject.GetComponent<Interactive>();
            MonoBehaviourPun s = PhotonView.Find(newSupportID).gameObject.GetComponent<MonoBehaviourPun>();
            print("AddOwner of object " + go.name + " to " + s.name);
            go.SetOwner(s);
        }

        [PunRPC]
        public void RemoveSupport(int interactiveID)
        {
            Interactive go = PhotonView.Find(interactiveID).gameObject.GetComponent<Interactive>();
            print("RemoveOwner of object " + go.name);
            go.RemoveOwner();
        }

        #endregion

        #region Private Methods

        #endregion
    }
}