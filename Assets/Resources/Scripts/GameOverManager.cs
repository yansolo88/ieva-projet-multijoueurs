﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class GameOverManager : MonoBehaviourPunCallbacks
{
    public AudioSource audio;

    private bool sceneIsLoading = false;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("WaitBeforeMainScreen");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator WaitBeforeMainScreen()
    {
        if(audio != null) {
            audio.Play();
            yield return new WaitForSeconds(audio.clip.length);
        } else {
            yield return new WaitForSeconds(15.0f);
        }

        if(PhotonNetwork.IsMasterClient && !sceneIsLoading) {
            PhotonNetwork.CloseConnection(PhotonNetwork.LocalPlayer);
            PhotonNetwork.LoadLevel("Launcher");
            sceneIsLoading = true;
        }
    }
}
